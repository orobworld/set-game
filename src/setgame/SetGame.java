package setgame;

import ilog.concert.IloException;

/**
 * SetGame attempts to find all possible "sets" from the game Set.
 *
 * This addresses a question on OR Stack Exchange:
 * https://or.stackexchange.com/questions/10820/how-to-generate-feasible
 * -sets-with-a-mip
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class SetGame {

  /** Dummy constructor. */
  private SetGame() { }

  /**
   * Solves for all possible sets in the game.
   * @param args the command line arguments (ignored)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Generate the card deck.
    Set deck = new Set();
    // Try brute force.
    long clock = System.currentTimeMillis();
    int count = deck.findAllSets();
    clock = System.currentTimeMillis() - clock;
    System.out.println("Brute force found " + count + " sets in "
                       + (0.001 * clock) + " seconds.");
    // Try the IP model using a callback.
    System.out.println("Trying the IP model with callback ...");
    clock = System.currentTimeMillis();
    try (IP ip = new IP(deck)) {
      int found = ip.solve2();
      clock = System.currentTimeMillis() - clock;
      System.out.println("Number of sets found = " + found);
      System.out.println("Time required = " + (0.001 * clock) + " seconds.");
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
    // Try the basic IP model.
    System.out.println("Trying the basic IP model ...");
    clock = System.currentTimeMillis();
    try (IP ip = new IP(deck)) {
      int found = 0;
      while (ip.solve()) {
        found += 1;
      }
      clock = System.currentTimeMillis() - clock;
      System.out.println("Number of sets found = " + found);
      System.out.println("Time required = " + (0.001 * clock) + " seconds.");
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
    // Try the CP model.
    System.out.println("Trying the CP model ...");
    clock = System.currentTimeMillis();
    try (CP cp = new CP(deck)) {
      int found = cp.solve();
      clock = System.currentTimeMillis() - clock;
      System.out.println("Number of sets found = " + found);
      System.out.println("Time required = " + (0.001 * clock) + " seconds.");
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
  }

}
